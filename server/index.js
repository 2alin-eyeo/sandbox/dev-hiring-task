const express = require("express");
const fs = require("fs");
const path = require("path");
const cors = require("cors");

/**
 * @namespace
 * @property {string} responseType - The type of response the server will deliver,
 * possible values are: "random", "error", "modified-data", "all-data"
 * @property {number} timer - Used in "random" responseType, the amount of milliseconds
 * that a new random respose will be served
 * @property {number} maxQuotes - Used in "modified-data" responseType, the maximum
 * number of quotes.
 * @property {boolean} setAuthorsAsUnknown - Used in "modified-data" responseType.
 * @property {boolean} removeTags - Used in "modified-data" responseType.
 */
let settings = {
  responseType: "random",
  timer: 3000,
};
// const settings = {
//   responseType: "modified",
//   maxQuotes: 100,
//   removeTags: true,
// };

const dataPath = "data/quotes.json";
const allQuotes = JSON.parse(fs.readFileSync(resolvePath(dataPath)));
let quotesResponse;
let randomIntervalId;
let randomResponseType;

init();

function init() {
  handleSettingsChanged();
}

function handleSettingsChanged() {
  clearInterval(randomIntervalId);

  if (settings.responseType === "random") {
    setRandomResponseType();
    randomIntervalId = setInterval(() => {
      setRandomResponseType();
      setQuotesResponse(randomResponseType);
    }, Number(settings.timer) || 3000);
  } else {
    setQuotesResponse(settings.responseType);
  }
}

function setQuotesResponse(responseType) {
  quotesResponse = JSON.parse(JSON.stringify(allQuotes));

  if (responseType === "all-data") return;

  // partial-data
  let {maxQuotes, setAuthorsAsUnknown, removeTags} =
    settings.responseType !== "random"
      ? settings
      : {
          maxQuotes: getRandomInteger(0, allQuotes.length),
          setAuthorsAsUnknown: getRandomBoolen(),
          removeTags: getRandomBoolen(),
        };

  if (maxQuotes === undefined || maxQuotes === null || maxQuotes === "")
    maxQuotes = allQuotes.length;

  // TODO: shuffle the array to deliver a random subset
  quotesResponse.splice(maxQuotes);

  for (const quote of quotesResponse) {
    if (setAuthorsAsUnknown) quote.author = "Unknown";
    if (removeTags) delete quote.tags;
  }
}

function setRandomResponseType() {
  const availableTypes = ["error", "modified-data", "all-data"];
  randomResponseType =
    availableTypes[getRandomInteger(0, availableTypes.length - 1)];
  // console.log("random response type changed to: ", randomResponseType);
}

function sendResponse(res, responseType) {
  if (responseType === "error") {
    res.status(500).send({error: "something went wrong"});
  } else {
    res.send(quotesResponse);
  }
}

// `min` and `max` are included
function getRandomInteger(min, max) {
  return Math.floor(Math.random() * (max + 1) + min);
}

function getRandomBoolen() {
  return Boolean(getRandomInteger(0, 1));
}

const app = express();
const port = 3000;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(express.static("public"));

app.get("/settings", (req, res) => {
  res.send(settings);
});

app.post("/settings", (req, res) => {
  settings = req.body;
  handleSettingsChanged();
  console.log("settings changed successfully ", settings);
  res.send(settings);
});

app.get("/quotes", (req, res) => {
  if (settings.responseType === "random") {
    sendResponse(res, randomResponseType);
  } else {
    sendResponse(res, settings.responseType);
  }
});

app.listen(port, () => {
  console.log(`Server running on http://localhost:${port}`);
});

function resolvePath(relativePath) {
  return path.join(__dirname, relativePath);
}
