# Installation
```bash
npm run install
```

# Running
```bash
node index.js
```
The server will run on `http://localhost:3000`.

# Configuration
While the server is running, open `http://localhost:3000/settings.html` to modify the kind of responses that the server will provide.

# Routes
* method: `GET`, route: `/quotes`
  - get all the quotes that the server provides based on its configuration

# Copyright disclaimer
Quotes extracted from the data used in https://github.com/dwyl/quotes.
